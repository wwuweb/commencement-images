#!/usr/bin/env python

import json
import os

from urllib.parse import urlparse
from urllib.request import urlopen, urlretrieve

endpoint = urlopen('https://commencement.wwu.edu/export/grad-recordings')
recordings = json.loads(endpoint.read())

try:
    os.mkdir('recordings')
except:
    pass

for recording in recordings:
    name = recording['name']
    uri = urlparse(recording['uri'])
    filename, extension = os.path.splitext(uri.path)

    print(name)
    print(uri)

    urlretrieve(uri, 'recordings/' + name + extension)
