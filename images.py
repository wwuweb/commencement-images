#!/usr/bin/env python

import json
import os

from urllib.request import urlopen, urlretrieve

endpoint = urlopen('https://commencement.wwu.edu/export/grad-images')
images = json.loads(endpoint.read())

try:
    os.mkdir('images')
except:
    pass

for image in images:
    name = image['name']
    uri = image['uri']

    print(name)
    print(uri)

    urlretrieve(uri, 'images/' + name + '.jpg')
